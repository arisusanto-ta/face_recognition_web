from django.apps import AppConfig


class MediaStreamConfig(AppConfig):
    name = 'media_stream'
