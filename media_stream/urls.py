from django.urls import path

from . import views

urlpatterns = [
    path('', views.cam_live, name='cam_live'),
]
