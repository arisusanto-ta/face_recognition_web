import cv2
import numpy as np
import dlib
from imutils import face_utils
from face_ai import utils


def recognize(face, image):
    (fX, fY, fW, fH) = face_utils.rect_to_bb(face)
    roi = image[fY:fY + fH, fX:fX + fW]
    roi = cv2.resize(roi, (96, 96))
    label, prediction, respond_time = utils.predict(roi)

    acc = prediction[np.argmax(prediction)] * 100

    prob = "%.2f" % acc
    respond_time = "%.2f" % respond_time

    opt = label + ' (' + str(prob) + '%) time: ' + str(respond_time) + 's'

    if acc > 0:
        cv2.putText(image, opt, (fX, fY - 10),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)

    cv2.rectangle(image, (fX, fY), (fX + fW, fY + fH),
                  (244, 212, 66), 1)

    return image


class VideoCamera(object):
    def __init__(self):
        self.video = cv2.VideoCapture(0)
        self.detector = dlib.get_frontal_face_detector()

    def __del__(self):
        self.video.release()

    def get_frame(self):
        success, image = self.video.read()
        image = cv2.flip(image, 1)
        #image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)

        faces = self.detector(image)

        if len(faces) > 0:
            for face in faces:
                try:
                    image = recognize(face, image)
                except:
                    print('error')

        ret, jpeg = cv2.imencode('.jpg', image)
        return jpeg.tobytes()
