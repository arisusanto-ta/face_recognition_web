from django.core.management.base import BaseCommand
from face_ai.deepnet.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import argparse
import os
import split_folders

from face_ai.data import load_metadata
import numpy as np
from pyprind import ProgBar

image_gen = ImageDataGenerator(rotation_range=0,
                               featurewise_center=True,
                               featurewise_std_normalization=True,
                               width_shift_range=0.1,
                               height_shift_range=0.1,
                               shear_range=0.01,
                               zoom_range=[0.9, 1.25],
                               horizontal_flip=True,
                               vertical_flip=False,
                               fill_mode='nearest',
                               data_format='channels_last',
                               brightness_range=[0.1, 2.0])

def load_images(metadata, height = 96,width = 96):
    img = np.empty((len(metadata), height, width, 3))
    i = 0
    for data in metadata:
        image = load_img(data.image_path())
        x = img_to_array(image)
        img[i] = x
        i += 1
    return img


def main(directory,output,total):
    metadata = load_metadata(directory)

    print(len(metadata))

    images = load_images(metadata)

    image_gen.fit(images)

    bar = ProgBar(len(metadata),bar_char='█')
    for data in metadata:
        img = load_img(data.image_path())
        x = img_to_array(img)
        x = x.reshape((1,) + x.shape)

        output_path = output + data.name
        if not os.path.exists(output_path):
            os.makedirs(output_path)

        i = 0
        for batch in image_gen.flow(x, batch_size=1,
                          save_to_dir=output_path, save_prefix=data.name, save_format='jpeg'):
            i += 1
            if i > total:
                break  # otherwise the generator would loop indefinitely  
        bar.update()

class Command(BaseCommand):
    help = 'Create augmentation from data'

    def add_arguments(self, parser):
        parser.add_argument('total', type=int,help='Indicates the number of augmented data to be created')

        # Optional argument
        parser.add_argument('-d', '--directory', type=str, help='Data source', )
        parser.add_argument('-o', '--output', type=str, help='Output directory', )

    def handle(self, *args, **kwargs):
        total = kwargs['total']
        directory = kwargs['directory']
        output = kwargs['output']
        
        main(directory,output,total)
        split_folders.ratio(output, output="data", seed=1337, ratio=(.8, .2))