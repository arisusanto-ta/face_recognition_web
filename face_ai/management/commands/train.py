from face_ai.deepnet.applications import VGG16
from django.core.management.base import BaseCommand

from face_ai.deepnet import layers
from face_ai.deepnet.function.regularizers import l2
from face_ai.deepnet.models import Model
from face_ai.deepnet.preprocessing.image import ImageDataGenerator, img_to_array, load_img

from face_ai.deepnet.function.callbacks import CSVLogger, ModelCheckpoint, EarlyStopping
from face_ai.deepnet.function.callbacks import ReduceLROnPlateau


def vgg16_finetuned(num_classes):
    conv_base = VGG16(weights='imagenet',
                      include_top=False,
                      input_shape=(96, 96, 3))

    # conv_base.trainable = False
    x = conv_base.output
    x = layers.Dropout(.4)(x)
    x = layers.Flatten()(x)

    prediction = layers.Dense(num_classes, kernel_initializer='glorot_uniform', kernel_regularizer=l2(.0005),
                              activation='softmax')(x)
    model = Model(input=conv_base.input, output=prediction)

    return model


def train_model(model, epochs):
    datagen = ImageDataGenerator(rescale=1. / 255)
    train_generator = datagen.flow_from_directory(
        directory='data/train',
        target_size=(96, 96),
        color_mode='rgb',
        batch_size=128,
        seed=11
    )

    validation_generator = datagen.flow_from_directory(
        directory='data/val',
        target_size=(96, 96),
        color_mode='rgb',
        batch_size=128,
        seed=11
    )

    # callbacks
    base_path = 'train_data/'
    patience = 50
    log_file_path = base_path + 'logs/training.log'
    csv_logger = CSVLogger(log_file_path, append=False)
    reduce_lr = ReduceLROnPlateau('val_loss', factor=0.1,
                                  patience=int(patience / 4), verbose=1)
    early_stop = EarlyStopping('val_loss', patience=patience)
    trained_models_path = base_path + 'models/_vgg16_'
    model_names = trained_models_path + '.{epoch:02d}-{val_acc:.2f}.hdf5'
    model_checkpoint = ModelCheckpoint(model_names, 'val_loss', verbose=1,
                                       save_best_only=True)
    callbacks = [model_checkpoint, csv_logger, reduce_lr, early_stop]
    model.compile(optimizer='adam', loss='categorical_crossentropy',
                  metrics=['accuracy'])

    model.fit_generator(
        generator=train_generator,
        validation_data=validation_generator,
        steps_per_epoch=train_generator.n // 128,
        validation_steps=validation_generator.n // 128,
        epochs=epochs,
        verbose=1,
        callbacks=callbacks
    )


class Command(BaseCommand):
    help = 'Train face recognition model'

    def add_arguments(self, parser):
        parser.add_argument('num_classes', type=int, help='Number of classes')
        parser.add_argument('epoch', type=int, help='Number of epoch')

        # Optional argument
        parser.add_argument('-o', '--option', type=str, help='Show options', )

    def handle(self, *args, **kwargs):
        num_classes = kwargs['num_classes']
        model = vgg16_finetuned(num_classes)

        if kwargs['option'] == 'print':
            model.summary()
        elif kwargs['option'] == 'start':
            train_model(model, kwargs['epoch'])
