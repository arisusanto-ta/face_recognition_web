"""Enables dynamic setting of underlying Keras module.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

_DEEPNET_BACKEND = None
_DEEPNET_UTILS = None


def set_deepnet_submodules(backend, utils):
    global _DEEPNET_BACKEND
    global _DEEPNET_UTILS
    _DEEPNET_BACKEND = backend
    _DEEPNET_UTILS = utils


def get_deepnet_submodule(name):
    if name not in {'backend', 'utils'}:
        raise ImportError(
            'Can only retrieve "backend" and "utils". '
            'Requested: %s' % name)
    if _DEEPNET_BACKEND is None:
        raise ImportError('You need to first `import keras` '
                          'in order to use `keras_preprocessing`. '
                          'For instance, you can do:\n\n'
                          '```\n'
                          'import keras\n'
                          'from keras_preprocessing import image\n'
                          '```\n\n'
                          'Or, preferably, this equivalent formulation:\n\n'
                          '```\n'
                          'from keras import preprocessing\n'
                          '```\n')
    if name == 'backend':
        return _DEEPNET_BACKEND
    elif name == 'utils':
        return _DEEPNET_UTILS
