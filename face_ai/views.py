from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from django.conf import settings
from django.core.files.storage import FileSystemStorage

import numpy as np
import cv2
import os

from . import utils


def index(request):
    data = {
        'status': 'ok'
    }

    return JsonResponse(data)

@csrf_exempt
def recognize(request):
    data = {
        'response': 'ok'
    }
    if request.method == 'POST':
        if len(request.FILES) != 0:
            myfile = request.FILES['image']

            #upload image
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            data['filename'] = myfile.name

            #preprocession
            path = 'media/' + filename
            img = cv2.imread(path)
            img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
            img = cv2.resize(img,(96,96))
            os.remove(path)

            label,prediction,respond_time = utils.predict(img)
            probability = "%.2f" % (prediction[np.argmax(prediction)] * 100)
            respond_time = "%.2f" % respond_time

            data['label'] = label
            data['probability'] = probability
            data['respond_time'] = respond_time
            
            return JsonResponse(data)
        else:
            data['response'] = 'Error: no files'
            return JsonResponse(data)
    
   
    return JsonResponse(data)


@csrf_exempt
def compare(request):
    data = {
        'response': 'ok'
    }

    if request.method == 'POST':

        if len(request.FILES) == 2:
            file1 = request.FILES['image1']
            file2 = request.FILES['image2']

            #upload image
            fs = FileSystemStorage()

            file1_name = fs.save(file1.name, file1)
            file2_name = fs.save(file2.name, file2)

            #preprocession
            img = np.empty((2, 96, 96, 3))

            path1 = 'media/' + file1_name
            path2 = 'media/' + file2_name
            img1 = cv2.imread(path1)
            img1 = cv2.resize(img1,(96,96))
            img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
            img2 = cv2.imread(path2)
            img2 = cv2.resize(img2,(96,96))
            img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
            img[0] = img1
            img[1] = img2

            os.remove(path1)
            os.remove(path2)

            label,predictions,respond_time = utils.predict_batch(img)

            similiarity = "%.2f" % (predictions[1][np.argmax(predictions[0])] * 100)
            respond_time = "%.2f" % respond_time
            label_1 = label[0]
            label_2 = label[1]

            data['similiarity'] = similiarity
            data['label_1'] = label_1
            data['label_2'] = label_2
            data['respond_time'] = respond_time
            return JsonResponse(data)

        else:
            data['response'] = 'Error: no files'
            return JsonResponse(data)

    return JsonResponse(data)
