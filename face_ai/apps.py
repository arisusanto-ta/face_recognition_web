from django.apps import AppConfig


class FaceAiConfig(AppConfig):
    name = 'face_ai'
