from django.shortcuts import render


def index(request):
    return render(request, "index.html")


def compare(request):
    return render(request, "compare.html")


def verify(request):
    return render(request, "verify.html")
