var result = document.getElementById("result");

	$("#img_preview1").click(function () {
       $('#img1').click();
    });

    $("#img_preview2").click(function () {
       $('#img2').click();
    });

    function img1_readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#img_preview1').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function img2_readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              $('#img_preview2').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('#img1').on( 'change', function () {
            img1_readURL(this);
    });
    $('#img2').on( 'change', function () {
            img2_readURL(this);
    });

    function verify(){
			var myform = document.getElementById("uploadFileForm");
            var fd = new FormData(myform);

						var error = false;
						var errMsg = '';

            var label_1 = '';
            var label_2 = '';
            var similiarity = '';
						var respond_time = '';

            $.ajax({
                url: "/api/compare",
                type: 'POST',
                data: fd,
                contentType: false,
                processData: false,
                beforeSend: function(){
                	Swal.fire({
                        title: 'Memverifikasi...',
                        html: 'Silahkan tunggu',
                        onBeforeOpen: () => {
                          Swal.showLoading()
                        }
                      });
                },
                success: function(response){
                  console.log(response)
									if (response.response != 'ok') {
										error = true;
										errMsg = response.response;
									} else{
                    label_1 = response.label_1;
                    label_2 = response.label_2;
										similiarity = response.similiarity + '%';
										respond_time = response.respond_time + 's';
									}
                },
								error: function (xhr, ajaxOptions, thrownError) {
									error = true;
									errMsg = xhr.status + ' : ' + thrownError
								},
                complete:function(data){
                  Swal.close();
									if (error == true) {
										Swal.fire({
											type: 'error',
											title: 'Verifikasi Gagal!',
											text: errMsg,
										});
									} else{
										result.style.display = 'block';
                    $('#label_1').html(label_1);
                    $('#label_2').html(label_2);
										$('#similiarity').html(similiarity);
										$('#respond_time').html(respond_time);
									}
                }
            });
    }