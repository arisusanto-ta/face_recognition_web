from vgg16 import VGG16
import split_folders
from face_ai.deepnet import layers, models
from face_ai.deepnet.preprocessing.image import ImageDataGenerator, load_img, img_to_array
import numpy as np
from face_ai.deepnet.function.callbacks import CSVLogger, ModelCheckpoint, EarlyStopping
from face_ai.deepnet.function.optimizers import SGD
from face_ai.deepnet.function.regularizers import l2
from face_ai.deepnet.function.callbacks import ReduceLROnPlateau
from face_ai.data import load_metadata

import argparse
import pickle
import os
from pyprind import  ProgBar

image_gen = ImageDataGenerator(rotation_range=0,
                               featurewise_center=True,
                               featurewise_std_normalization=True,
                               width_shift_range=0.1,
                               height_shift_range=0.1,
                               shear_range=0.01,
                               zoom_range=[0.9, 1.25],
                               horizontal_flip=True,
                               vertical_flip=False,
                               fill_mode='nearest',
                               data_format='channels_last',
                               brightness_range=[0.1, 2.0])

def load_images(metadata, height=96, width=96):
    img = np.empty((len(metadata), height, width, 3))
    i = 0
    for data in metadata:
        image = load_img(data.image_path())
        x = img_to_array(image)
        img[i] = x
        i += 1
    return img

metadata = load_metadata('train_data/images')
images = load_images(metadata)
image_gen.fit(images)

def create_test_data():
    output = 'test_data/images/'
    test_metadata = load_metadata('train_data/dataset/train')
    print(len(test_metadata))
    bar = ProgBar(len(test_metadata), bar_char='█')
    for data in metadata:
        img = load_img(data.image_path())
        x = img_to_array(img)
        x = x.reshape((1,) + x.shape)

        output_path = output + data.name
        if not os.path.exists(output_path):
            os.makedirs(output_path)

        i = 0
        for batch in image_gen.flow(x, batch_size=1,
                                    save_to_dir=output_path, save_prefix=data.name, save_format='jpeg'):
            i += 1
            if i > 64:
                break  # otherwise the generator would loop indefinitely
        bar.update()


def create_label_encoder():
    images_path = 'train_data/images'
    cls_name = []
    for i in sorted(os.listdir(images_path)):
        cls_name.append(i)

    pickle.dump(cls_name, open('face_ai/models/label_encoder', 'wb'))

    return cls_name


def create_model(num_classes):
    conv_base = VGG16(weights='imagenet',
                      include_top=False,
                      input_shape=(96, 96, 3))

    # for layer in conv_base.layers[:-4]:
    #     layer.trainable = False
    conv_base.trainable = False

    model = models.Sequential()
    model.add(conv_base)
    model.add(layers.Flatten())
    model.add(layers.Dense(1024, activation='relu'))
    model.add(layers.Dropout(0.5))
    model.add(layers.Dense(num_classes, kernel_regularizer=l2(.0005), activation='softmax'))

    return model


def create_data_generator(train_dir, test_dir, batch_size):

    data_gen = ImageDataGenerator(rotation_range=0,
                               featurewise_center=False,
                               featurewise_std_normalization=False,
                               horizontal_flip=True,
                               vertical_flip=False,
                               data_format='channels_last')

    train_data = image_gen.flow_from_directory(
        directory=train_dir,
        target_size=(96, 96),
        color_mode='rgb',
        batch_size=batch_size,
        seed=11
    )
    validation_data = image_gen.flow_from_directory(
        directory=test_dir,
        target_size=(96, 96),
        color_mode='rgb',
        batch_size=batch_size,
        seed=11
    )

    return train_data, validation_data


def train_model(model, epochs, batch_size):
    train_data, validation_data = create_data_generator('test_data/train',
                                                        'test_data/val',
                                                        batch_size)

    # callbacks
    base_path = 'train_data/'
    patience = 50
    log_file_path = base_path + 'logs/training.log'
    csv_logger = CSVLogger(log_file_path, append=False)
    reduce_lr = ReduceLROnPlateau('val_loss', factor=0.1,
                                  patience=int(patience / 4), verbose=1)
    early_stop = EarlyStopping('val_loss', patience=patience)
    trained_models_path = base_path + 'models/_vgg16_'
    model_names = trained_models_path + '.{epoch:02d}-{val_acc:.2f}.hdf5'
    model_checkpoint = ModelCheckpoint(model_names, 'val_loss', verbose=1,
                                       save_best_only=True)
    callbacks = [model_checkpoint, csv_logger, reduce_lr, early_stop]

    opt = SGD(lr=.01, momentum=.9)
    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])

    model.fit_generator(
        generator=train_data,
        validation_data=validation_data,
        steps_per_epoch=train_data.n // batch_size,
        validation_steps=validation_data.n // batch_size,
        epochs=epochs,
        verbose=1,
        callbacks=callbacks
    )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Train face recognition model')
    parser.add_argument('--numclasses', '-n', type=int, default=11,
                        help='Number of classes')
    parser.add_argument('--batchsize', '-b', type=int, default=64,
                        help='Number of images in each mini-batch')
    parser.add_argument('--epochs', '-e', type=int, default=1000,
                        help='Number of sweeps over the dataset to train')
    parser.add_argument('--option', '-o', default='start_train',
                        help='Option for vgg16')
    args = parser.parse_args()

    model = create_model(args.numclasses)
    if args.option == 'model_details':
        model.summary()
    elif args.option == 'split':
        split_folders.ratio('train_data/images', output="train_data/dataset", seed=1337, ratio=(.8, .2))
    elif args.option == 'start_train':
        train_model(model, args.epochs, args.batchsize)
    elif args.option == 'encode_label':
        cls_name = create_label_encoder()
        print(f'{len(cls_name)} Labels encoded')
        print(cls_name)
    elif args.option == 'create_test':
        create_test_data()
    else:
        print(f'Error: No option name: {args.option}')